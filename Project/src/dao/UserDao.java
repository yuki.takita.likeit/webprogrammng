package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javaBeans.user;

public class UserDao {
	//TODO ログインデータの参照
	public user findByInfo(String login_id, String password) {
		Connection con = null;

		try {
			//DB接続
			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? and password = md5(?)";

			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, login_id);
			pstmt.setString(2, password);
			ResultSet rs = pstmt.executeQuery();

			//falseの時にnullを返す
			if (!rs.next()) {
				return null;
			}
			String login_idData = rs.getString("login_id");
			String name = rs.getString("name");
			user User = new user(login_idData, name);
			return User;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//TODO 参照するデータを取得する
	public List<user> find() {
		Connection con = null;
		//user型リストの作成
		List<user> userList = new ArrayList<user>();

		try {
			//DB接続
			con = DBManager.getConnection();
			String sql = "SELECT login_id ,name,birth_date,create_date,update_date FROM user ";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				String birth_date = rs.getString("birth_date");
				String create_date = rs.getString("create_date");
				String update_date = rs.getString("update_date");
				user User = new user(login_id, name, birth_date, create_date, update_date);
				userList.add(User);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			//エラーの時はnullを返す。
			return null;
		} finally {
			//必ずDBを閉じる
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	//すべてのテーブルデータを取得するメソッド
	public List<user> findAll() {
		//TODO 全データの参照
		Connection con = null;
		//user型リストの作成
		List<user> userList = new ArrayList<user>();

		try {
			//DB接続
			con = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE login_id != 'admin'";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				String birth_date = rs.getString("birth_date");
				String password = rs.getString("password");
				String create_date = rs.getString("create_date");
				String update_date = rs.getString("update_date");
				user User = new user(id, login_id, name, birth_date, password, create_date, update_date);
				userList.add(User);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			//エラーの時はnullを返す。
			return null;
		} finally {
			//必ずDBを閉じる
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}
//検索したレコードを表示する。
	public List<user> findSearch(String loginIdP, String userNameP ,String startDate, String endDate) {
		//TODO 全データの参照
		Connection con = null;
		//user型リストの作成
		List<user> userList = new ArrayList<user>();

		try {
			//DB接続
			con = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE login_id != 'admin'";

			if(!(loginIdP.equals("")))
				{if(!(userNameP.equals(""))) {
					sql += " AND login_id = '" + "admin" + "';";
				}else{
					sql += " AND login_id = '" + loginIdP + "';";
				}
			}else if(!(userNameP.equals(""))){
				if(!(loginIdP.equals(""))) {
					sql += " AND name =  '" +"管理者"+ "';";
				}else {
					sql += " AND name LIKE '%" +userNameP+ "%';";
				}
			}else if(!(startDate.equals("")))
				{if(!(endDate.equals(""))) {
					sql += "AND birth_date BETWEEN cast('" + startDate + "' as date) and cast('" + endDate + "' as date);";
				}else{
					sql += " AND birth_date >= cast('" +startDate+"' as date);";}
			}else if(!(endDate.equals(""))){
				{if(!(startDate.equals(""))) {
					sql += "AND birth_date BETWEEN cast('"+ startDate +"' as date) and cast('" + endDate + "' as date);";
				}else{
					sql += " AND birth_date <= cast('" +endDate+ "' as date);";
				}}
			}
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				String birth_date = rs.getString("birth_date");
				String password = rs.getString("password");
				String create_date = rs.getString("create_date");
				String update_date = rs.getString("update_date");
				user User = new user(id, login_id, name, birth_date, password, create_date, update_date);
				userList.add(User);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			//エラーの時はnullを返す。
			return null;
		} finally {
			//必ずDBを閉じる
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	//nameで検索し、対象idを検索する
	public List<Integer> findByInfoNameList(String name) {
		Connection con = null;
		List<Integer> userList = new ArrayList<Integer>();
		try {
			//DB接続
			con = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE name = ?;";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, name);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				userList.add(id);
			}

			return userList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//nameで検索結果(部分一致)を返すメソッド
	public user findByInfoId(String Id) {
		Connection con = null;

		try {
			//DB接続
			con = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE id = ?";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, Id);
			ResultSet rs = pstmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int id = rs.getInt("id");
			String login_id = rs.getString("login_id");
			String name = rs.getString("name");
			String birth_date = rs.getString("birth_date");
			String password = rs.getString("password");
			String create_date = rs.getString("create_date");
			String update_date = rs.getString("update_date");
			user User = new user(id, login_id, name, birth_date, password, create_date, update_date);

			return User;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
//login_idで検索し、対象idを割り出す
	public List<Integer> findByInfoLoginIdList(String loginId) {
		Connection con = null;
		List<Integer> userList = new ArrayList<Integer>();
		try {
			//DB接続
			con = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE login_id = ?;";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, loginId);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				userList.add(id);
			}
			return userList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//誕生日で検索結果(範囲)を返すメソッド
	public List<Integer> findByInfoBirthList(String birth_dateMin, String birth_dateMax) {
		Connection con = null;
		List<Integer> userList = new ArrayList<Integer>();
		try {
			//DB接続
			con = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE birth_date BETWEEN cast(? as date) and cast(? as date);";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, birth_dateMin);
			pstmt.setString(2, birth_dateMax);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				userList.add(id);
			}

			return userList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//取得データをテーブルに登録するメソッド
	public int SignUp(String login_id, String name, String birth_date, String password,String passwordConf) {
		Connection con = null;
		try {
			if(!(password.equals(passwordConf))){
				return 0;
				}
			//DB接続
			con = DBManager.getConnection();
			String sql = "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date)values( ? , ? , cast(? as date),md5(?),cast(now() as date),cast(now() as datetime));";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, login_id);
			pstmt.setString(2, name);
			pstmt.setString(3, birth_date);
			pstmt.setString(4, password);
			int count = pstmt.executeUpdate();
			return count;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//データを更新するメソッド
	public int Update(String login_id,String password,String passwordConf,String name,String birth_date,String id) {
		Connection con = null;
		try {
			if(!(password.equals(passwordConf))){
				return 0;
				}
			//DB接続
			con = DBManager.getConnection();
			String sql = "UPDATE user set login_id=?,password=md5(?),name=?,birth_date=cast(? as date),update_date=cast(now() as datetime) Where id = ?;";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, login_id);
			pstmt.setString(2, password);
			pstmt.setString(3, name);
			pstmt.setString(4, birth_date);
			pstmt.setString(5, id);
			int count = pstmt.executeUpdate();

			return count;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//データの削除をする。
	public int Delete(String id) {
		Connection con = null;
		try {
			//DB接続
			con = DBManager.getConnection();
			String sql = "DELETE FROM user WHERE id=?;";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, id);
			int count = pstmt.executeUpdate();
			return count;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public List<user> findById(List<Integer> id){
			Connection con = null;
			//user型リストの作成
			List<user> userList = new ArrayList<user>();
			Iterator<Integer> iterator = id.iterator();
			try {
				while(iterator.hasNext()) {
					int idNext = iterator.next();
					//DB接続
					con = DBManager.getConnection();
					String sql = "SELECT * FROM user WHERE id = ? ;";
					PreparedStatement pstmt = con.prepareStatement(sql);
					pstmt.setInt(1,idNext);
					ResultSet rs =pstmt.executeQuery();

					while (rs.next()) {
						int id1 = rs.getInt("id");
						String login_id = rs.getString("login_id");
						String name = rs.getString("name");
						String birth_date = rs.getString("birth_date");
						String create_date = rs.getString("create_date");
						String update_date = rs.getString("update_date");
						user User = new user(id1,login_id, name, birth_date, create_date, update_date, update_date);
						userList.add(User);
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
				//エラーの時はnullを返す。
				return null;
			} finally {
				//必ずDBを閉じる
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		return userList;
	}
	public List<Integer> findByInfoBirthListStart(String birth_dateMin) {
		Connection con = null;
		List<Integer> userList = new ArrayList<Integer>();
		try {
			//DB接続
			con = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE birth_date >= cast(? as date);";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, birth_dateMin);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				userList.add(id);
			}

			return userList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public List<Integer> findByInfoBirthListEnd(String birth_dateMax) {
		Connection con = null;
		List<Integer> userList = new ArrayList<Integer>();
		try {
			//DB接続
			con = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE birth_date <= cast(? as date);";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, birth_dateMax);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				userList.add(id);
			}

			return userList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
