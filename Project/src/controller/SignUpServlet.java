package controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;

/**
 * Servlet implementation class SignUpServlet
 */
@WebServlet("/SignUpServlet")
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Date createDate = null;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUpServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO ログインセッションがない場合loginServletにリダイレクト
		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo")==null) {
			response.sendRedirect("loginServlet");
			return;
		}
		// TODO jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewUser.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コード指定
		request.setCharacterEncoding("UTF-8");

		//TODO 新規登録データの取得
		String login_id =request.getParameter("login_id");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String passwordConf=request.getParameter("password_conf");
		String birth_date =request.getParameter("birth_date");

		//TODO 新規登録
		UserDao userDao = new UserDao();
		int count = userDao.SignUp(login_id,name,birth_date,password,passwordConf);

		//TODO 返り値が0の場合エラーメッセージをセッションにセットする
		if(count==0) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/NewUser.jsp");
			dispacher.forward(request, response);
			return;
		}else if(count >= 1) {
		response.sendRedirect("UserListServlet");
		}
	}
}
