package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import javaBeans.user;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO ログインセッションがない場合loginServletにリダイレクト
		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo")==null) {
			response.sendRedirect("loginServlet");
			return;
		}
		//TODO IDを取得、検索、セッションにセット
		String id = request.getParameter("id");
		UserDao userDao = new UserDao();
		user user = userDao.findByInfoId(id);

		//TODO jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Update.jsp");
		dispatcher.forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コード指定
		request.setCharacterEncoding("UTF-8");

		//TODO 入力データの取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordConf=request.getParameter("passwordConf");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String id = request.getParameter("id");
		UserDao userDao = new UserDao();

		//TODO レコードの更新
		int Update = userDao.Update(loginId, password,passwordConf,name, birthDate,id);

		//TODO 更新できなかった場合はerrMsgをセット
		if(Update==0) {
			String errMsg = "入力された内容は正しくありません。";
			HttpSession session = request.getSession();
			session.setAttribute("UpdateErrmsg",errMsg);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Update.jsp");
			dispatcher.forward(request, response);
			return;
		}else if (Update>=1){
			response.sendRedirect("UserListServlet");
		}
	}
}
