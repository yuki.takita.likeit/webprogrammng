package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import javaBeans.user;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO ログインセッションがない場合loginServletにリダイレクト
		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo")==null) {
			response.sendRedirect("loginServlet");
			return;
		}
		//TODO 削除するレコードのIDを取得
		String id = request.getParameter("id");

		//TODO 削除するレコードのを検索してセッションにセットする。
		UserDao userDao = new UserDao();
		user user = userDao.findByInfoId(id);
		session.setAttribute("userDetail", user);

		//TODO jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Delete.jsp");
		dispatcher.forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//TODO hiddenparameterで送られてきた値で削除を実行する
		//TODO 削除実行＝１．キャンセル＝０
		String id = request.getParameter("id");
		String select = request.getParameter("select");
		UserDao userDao = new UserDao();
		if(select.equals("1")) {
			userDao.Delete(id);
			response.sendRedirect("UserListServlet");
		}else if(select.equals("0")) {
			response.sendRedirect("UserListServlet");
		}else {
			response.sendRedirect("UserListServlet");
		}
	}
}
