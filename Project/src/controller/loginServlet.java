package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import javaBeans.user;

/**
 * Servlet implementation class loginServlet
 */
@WebServlet("/loginServlet")
public class loginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public loginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    //doget 情報取得、検索
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//TODO ログインセッションがない場合loginServletにリダイレクト
		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo")!=null) {
			response.sendRedirect("UserListServlet");
			return;
		}

		//TODO フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	//dopost 情報の登録,fromに入力した情報を送信する。
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コード指定
		request.setCharacterEncoding("utf-8");

		//TODO リクエストパラメータの取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		UserDao UserDao  = new UserDao();

		//TODO DBにlogin_id,passwordで検索
		user user = UserDao.findByInfo(loginId,password);

		//TODO findByinfoでデータがない場合はエラーメッセ時をfowardする
		if(user == null) {
			request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");
			RequestDispatcher dispacher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispacher.forward(request, response);
			return;
		}

		//TODO セッションにログインデータをセットする
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user);

		//TODO UserListServletにリダイレクトさせる
		response.sendRedirect("UserListServlet");
	}
}
