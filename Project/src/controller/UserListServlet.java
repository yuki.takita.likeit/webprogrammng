package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import javaBeans.user;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");
		//TODO ログインセッションがない場合ログイン画面にリダイレクトさせる
		try {

			//TODO ログインセッションがない場合loginServletにリダイレクト
			HttpSession session = request.getSession();
			if(session.getAttribute("userInfo")==null) {
				response.sendRedirect("loginServlet");
				return;
			}

			//TODO ユーザー一覧情報を取得してセットする
			UserDao userDao = new UserDao();

			//TODO ユーザー型リストにユーザーデータを入れる。
			List<user> userList = userDao.findAll();
			request.setAttribute("userList", userList);

			//TODO jspにforward
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserList.jsp");
			dispatcher.forward(request, response);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

/*		//TODO object型のリストにゲットパラメータの値を入れる
		//0_login_id 1_name 2_date-start 3_date-end
		List<Object> List = new ArrayList<Object>();
		List.add(request.getParameter("login_id"));
		List.add(request.getParameter("name"));
		List.add(request.getParameter("date-start"));
		List.add(request.getParameter("date-end"));

		//TODO スイッチのリスト作成
		List<Integer> num = new ArrayList<Integer>();
		for(int i = 0 ; i<4 ; i++) {
			num.add(i);
		}

		//TODO 検索するIDを格納するListを作成
		UserDao userDao = new UserDao();
		List<Integer> userId = new ArrayList<Integer>();

		//TODO どれがnullか特定する
		for(int i = 0 ; i<4 ;i++) {
			if(List.get(i).equals(null)) {
				num.remove(i);
			}
		}


		//TODO swichでカラムごとに検索する
		try {
			for(int i = 0 ; i < num.size() ; i++ ) {
				switch(num.get(i)) {
					case 0:
						String login_id = request.getParameter("login_id");
						userId.addAll(userDao.findByInfoLoginIdList(login_id));
						continue;
					case 1:
						String name = request.getParameter("name");
						userId.addAll(userDao.findByInfoNameList(name));
						continue;
					case 2:
						if(num.size()==3) {
							String dateStart = request.getParameter("date-start");
							String dateEnd = request.getParameter("date-end");
							userId.addAll(userDao.findByInfoBirthList(dateStart, dateEnd));
						continue;
						}else {
							String dateStart = request.getParameter("date-start");
							userId.addAll(userDao.findByInfoBirthListStart(dateStart));
						continue;
						}
					case 3:
							String dateEnd = request.getParameter("date-end");
							userId.addAll(userDao.findByInfoBirthListEnd(dateEnd));
						continue;
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
			}

		//TODO 重複IDの削除、リストの整列
		List<Integer> user = new ArrayList<Integer>(new HashSet<Integer>(userId));
		Collections.sort(user);*/

		String loginIdP = request.getParameter("login_id");
		String userNameP = request.getParameter("name");
		String startDate =request.getParameter("date-start");
		String endDate =request.getParameter("date-end");

		//TODO 検索結果をセッションにセット
		UserDao UserDao = new UserDao();
		List<user> userList = UserDao.findSearch(loginIdP, userNameP, startDate, endDate);

		HttpSession session = request.getSession();
		session.setAttribute("userListResult", userList);

		//TODO jspにforward
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserList.jsp");
		dispatcher.forward(request, response);
	}
}
