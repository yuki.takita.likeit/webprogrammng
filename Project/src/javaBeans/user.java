package javaBeans;

import java.io.Serializable;

//データの直列化
public class user implements Serializable {
	private int id;
	private String loginId;
	private String name;
	private String birthDate;
	private String password;
	private String createDate;
	private String updateDate;


	public user(int id) {
		this.id = id;
	}
//ログインセッション保存用のコンストラクタの作成
	public user(String loginId,String name) {
		this.name=name;
		this.loginId = loginId;
	}
//参照用のコンストラクタの作成
	public user(String loginId, String name, String birthDate, String createDate, String updateDate) {
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}
//新規登録用のコンストラクタの作成
	public user(String loginId, String name, String birthDate, String createDate) {
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.createDate = createDate;
	}
//取得データ保存用のコンストラクタの作成
	public user(int id, String loginId, String name ,String birthDate, String password, String createDate, String updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}



}