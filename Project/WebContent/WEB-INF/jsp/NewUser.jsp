<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang=ja>
<head>
<meta charset="UTF-8">
<title>新規登録</title>
    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- オリジナルCSS読み込み -->
    <link href="css/NewUser.css" rel="stylesheet">
    <!-- Jqeryの読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    </script>
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js">
    </script>
    <!-- レイアウトカスタマイズ用個別CSS -->
</head>
    <body>
    <!--header-->
    <header>
        <nav class="navbar">
            <a class="navbar_link" href="SignUpServlet">新規登録ページ</a>
        </nav>
    </header>
    <!--/header-->
    	<div class="form">
        <form action="SignUpServlet" method="post">
            <c:if test="${errMsg != null}">
                <div class="errMsg">
                ${errMsg}
                </div>
            </c:if>
            <div class="col-sm-5">ログインID</div>
			<div class="col-sm-5"><input type="text" name="login_id" id="loginId" placeholder="ログインID" style="width:200px;" class="form-control" required autofocus></div>
	        <div class="col-sm-5">パスワード</div>
	        <div class="col-sm-5"><input type="password" name="password" id="password" placeholder="パスワード" style="width:200px;" class="form-control" autofocus></div>
	        <div class="col-sm-5">パスワード(確認)</div>
	        <div class="col-sm-5"><input type="password" name="password_conf" id="password" placeholder="パスワード(確認)" style="width:200px;" class="form-control" autofocus></div>
	        <div class="col-sm-5">ユーザー名</div>
	        <div class="col-sm-5"><input type="text" name="name" id="name" placeholder="名前" style="width:200px;" class="form-control" required autofocus></div>
	        <div class="col-sm-5">生年月日</div>
	        <div class="col-sm-5"><input type="text" name="birth_date" id="birthDate" placeholder="19990101" style="width:200px;" class="form-control" required autofocus></div>
	        <div class="col-sm-12"><br><input type="submit" name="送信" style="color:black;"  class="btn btn-primay" value="submit"></div>
        </form>
        </div>
    </body>
</html>