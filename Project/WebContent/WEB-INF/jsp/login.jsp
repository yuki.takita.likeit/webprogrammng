<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title>ログイン画面</title>
        <!-- BootstrapのCSS読み込み -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- オリジナルCSS読み込み-->
        <link href="css/login.css" rel="stylesheet">
        <!-- Jqeryの読み込み -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- BootstrapのJS読み込み -->
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <!-- header  -->
           <header>
        <div class="bg">
            <!--エラーメッセージの表示 -->
            <c:if test="${errMsg != null}">
              <div role="alert" class="errMsg">
                  ${errMsg}
              </div>
            </c:if>
        <div class ="conteiner">
<!--            <div class="icon">
                <img  src="jpeg/icon.jpeg">
            </div>-->
            <div class="info">
                <form action="loginServlet" method="post">
                    <table>
                        <tr>
                            <td class="login"><p>Login Id</p></td>
                            <td><p><input type="text" class="loginId" name="loginId" ></p></td>
                        </tr>
                        <tr>
                            <td class="pass"><p>Password</p></td>
                            <td><p><input type="password" class="password" name="password"></p></td>
                            <td><input type = "submit" value="Submit"  class="submit" ></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        </div>
        </header>
    </body>
</html>