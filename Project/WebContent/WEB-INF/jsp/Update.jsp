<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang=ja>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/Update.css" rel="stylesheet">
	<title>ユーザー更新</title>
</head>
<body>
	<h1>ID:${userDetail.id}</h1>
	<div class="errMsg">
    <c:if test="${UpdateErrmsg!=null}">
    ${UpdateErrmsg}
    </c:if>
    </div>
	<div class="form">
		<form action="UserUpdateServlet" method="post">
			<input type="hidden" name="id" value="${userDetail.id}">
			<div class="col-sm-5">ログインID</div>
			<div class="col-sm-5">${userDetail.loginId}</div>
	        <div class="col-sm-5">パスワード</div>
	        <div class="col-sm-5"><input type="password" name="password" id="password" placeholder="パスワード" style="width:200px;" class="form-control" autofocus></div>
	        <div class="col-sm-5">パスワード(確認)</div>
	        <div class="col-sm-5"><input type="password" name="passwordConf" id="password" placeholder="パスワード(確認)" style="width:200px;" class="form-control" autofocus></div>
	        <div class="col-sm-5">ユーザー名</div>
	        <div class="col-sm-5"><input type="text" name="name" id="name" value="${userDetail.name}" style="width:200px;" class="form-control" required autofocus></div>
	        <div class="col-sm-5">生年月日</div>
	        <div class="col-sm-5"><input type="text" name="birthDate" id="birthDate" value="${userDetail.birthDate}" style="width:200px;" class="form-control" required autofocus></div>
	        <div class="col-sm-12"><br><input type="submit" name="送信" style="color:black;"  class="btn btn-primay" value="submit"></div>
		</form>
	</div>
</body>
</html>